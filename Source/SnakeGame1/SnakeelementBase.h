// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeelementBase.generated.h"


class UStaticMeshComponent;


UCLASS()

class SNAKEGAME1_API ASnakeelementBase : public AActor
{
	GENERATED_BODY()
	

		// Sets default values for this actor's properties
	ASnakeelementBase();

	UPROPERTY (VisibleAnyWhere)

		UStaticMeshComponent* MeshComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
